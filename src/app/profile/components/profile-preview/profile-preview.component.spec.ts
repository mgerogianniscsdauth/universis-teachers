import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePreviewComponent } from './profile-preview.component';
import { NgArrayPipesModule } from 'angular-pipes';
import { NgPipesModule } from 'ngx-pipes';
import { ProfileService } from '../../services/profile.service';
import { ErrorService } from '@universis/common';

describe('ProfilePreviewComponent', () => {
  let component: ProfilePreviewComponent;
  let fixture: ComponentFixture<ProfilePreviewComponent>;

  const profileSvc = jasmine.createSpyObj('ProfleService,', ['getInstructor']);
  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);

  profileSvc.getInstructor.and.returnValue(Promise.resolve(JSON.parse('{}')));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgArrayPipesModule,
        NgPipesModule
      ],
      declarations: [ ProfilePreviewComponent ],
      providers: [
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
