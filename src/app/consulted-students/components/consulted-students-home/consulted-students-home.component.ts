import { Component, OnDestroy, OnInit } from '@angular/core';
import * as CONSULTED_STUDENTS_LIST_CONFIG from '../consulted-students-table/consulted-students-table.config.list.json';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-consulted-students-home',
  templateUrl: './consulted-students-home.component.html',
  styleUrls: ['./consulted-students-home.component.scss']
})
export class ConsultedStudentsHomeComponent implements OnInit, OnDestroy {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.paths = (<any>CONSULTED_STUDENTS_LIST_CONFIG).paths;
    this.activePaths = this.paths.filter( x => {
      return x.show === true;
    }).slice(0);
    this.paramSubscription = this._activatedRoute.firstChild.params.subscribe( params => {
      const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });
  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
