import {Component, Input, OnInit} from '@angular/core';
import { I18nPluralPipe } from '@angular/common';


@Component({
  selector: 'app-courses-grades-step-2',
  templateUrl: './courses-grades-step-2.component.html',
  styles: [`
      .list-unsuccess-grades {
        max-height: 120px;
        overflow-y: scroll;
        overflow-x: hidden;
      }
    `]
})
export class CoursesGradesStep2Component implements OnInit {

  @Input() resultsOfUpload;   // data with results
  @Input() courseExam;        // current courseExam object
  @Input() counters;          // counters with sums for every grade result state
  @Input() message = '';
  @Input() errorMessage = '';
  @Input() isLoading = false;


  gradesUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.none',
    '=1': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.singular',
    'other': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.plural'
  };

  gradesNotUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.none',
    '=1': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.singular',
    'other': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.plural'
  };

  constructor() { }

  ngOnInit() {

  }
}
